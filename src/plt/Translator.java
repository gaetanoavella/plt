package plt;

public class Translator {
	
	private String phrase;
	private String subPhrase;
	public static final String NIL = "nil";
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}
	
	public String translate() {
		String punctuactions ="";
		String[] words = phrase.split("(?= )|(?=-)|(?=!)|(?=,)|(?=\\()|(?=\\.)|(?=\\:)|(?=\\')|(?=\\?)|(?=\\;)");
		
		for (int i = 0; i<words.length; i++) {	
			phrase = words[i];
			if (startWithPunctuactions(phrase)) {
				punctuactions = phrase.substring(0,1);
				phrase = phrase.substring(1);
				words[i] = punctuactions + translateSingleWord (phrase);	
				}
			else {
				words[i] = translateSingleWord (phrase);
				}
			}
		return String.join("", words);
		}
	
	
	public String translateSingleWord(String phrase) {
		if (!phrase.equals("")) {
			if (numberOfInitialConsonants() == 0 ) {
				return NumberOfConsonantIsZero();
				} else if (numberOfInitialConsonants() != 0) {
					return numberOfConsonantOtherThanZero();
					}
			} 
		return NIL;
		}
	
	
	private String NumberOfConsonantIsZero() {
		String translation ="";
		if (phrase.endsWith("y")) {
			translation = phrase + "nay";
			} else if (endWithVowel()) {
				translation = phrase + "yay";
				} else if (endWithConsonant()) {
					translation = phrase + "ay";
					}
		return translation;
		}
	
	//Il metodo non � implementato in modo ricorsivo perch� si assume che non esistano parole che comincino con piu di 4 consonanti consecutive
	private String numberOfConsonantOtherThanZero() {
		String translation ="";
		if (numberOfInitialConsonants() == 1) {
			translation = phrase.substring(1) + phrase.charAt(0) + "ay";	
			} else if  (numberOfInitialConsonants() == 2) {
				translation = phrase.substring(2) + phrase.substring(0,2) + "ay";	
				} else if  (numberOfInitialConsonants() == 3) {
					translation = phrase.substring(3) + phrase.substring(0,3) + "ay";	
					} else if  (numberOfInitialConsonants() == 4) {
						translation = phrase.substring(4) + phrase.substring(0,4) + "ay";	
						}
		return translation; 
		}
	
	
	private boolean startWithPunctuactions(String stringa) {
		return (stringa.startsWith(" ") || stringa.startsWith("-")|| stringa.startsWith("!") || stringa.startsWith(".") || stringa.startsWith(",") || stringa.startsWith(";") || stringa.startsWith(":") || stringa.startsWith("?") || stringa.startsWith("'") || stringa.startsWith("(") );
	}
	
	private boolean startWithVowel() {
		return (subPhrase.startsWith("a") || subPhrase.startsWith("e") || subPhrase.startsWith("i") || subPhrase.startsWith("o") || subPhrase.startsWith("u"));
	}

	private boolean endWithVowel() {
		return (phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	private boolean endWithConsonant() {
		return !(phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"));
	}
	
	
	private int numberOfInitialConsonants() {
		int initialConsonants = 0;
		for (int i=0; i<phrase.length(); i++) {
			subPhrase = phrase.substring(i);
			
			if (!startWithVowel()) {
				initialConsonants++;
				} else break;
			}
		return initialConsonants;
		}
}
