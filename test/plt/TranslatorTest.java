package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator (inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator (inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test
	public void testTranslationPhraseStartingWithAendingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator (inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator (inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator (inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator (inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingolConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithY() {
		String inputPhrase = "yellow";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellowyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithTwoConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithThreeConsonant() {
		String inputPhrase = "split";
		Translator translator = new Translator (inputPhrase);
		assertEquals("itsplay", translator.translate());
	}

	@Test
	public void testTranslationPhraseWithTwoWords() {
		String inputPhrase = "hello world";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testTranslationCompoundWorld() {
		String inputPhrase = "well-being";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithExclamationMark() {
		String inputPhrase = "hello!friend";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay!iendfray", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithPoint() {
		String inputPhrase = "hello.friend";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay.iendfray", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithComma() {
		String inputPhrase = "hello,friend";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay,iendfray", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithSemiColon() {
		String inputPhrase = "hello;friend";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay;iendfray", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithQuestionMark() {
		String inputPhrase = "hello?friend";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay?iendfray", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithApostrophe() {
		String inputPhrase = "hello'friend";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay'iendfray", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithRoundBracket() {
		String inputPhrase = "hello(friend";
		Translator translator = new Translator (inputPhrase);
		assertEquals("ellohay(iendfray", translator.translate());
	}
	
	
}
